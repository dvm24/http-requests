package dvm.json;

import android.content.SharedPreferences;
import android.nfc.Tag;
import android.text.Editable;
import android.util.JsonReader;
import android.util.Log;
import android.util.Xml;
import android.widget.TextView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class HttpR {
    public static String session = "none";
    public static int uid;

    private static String API = new String("http://192.168.1.51/API/");
    public static String send(ArrayList data, String url){//Получаем список данных
        try {
            HttpClient httpclient = new DefaultHttpClient();

            HttpPost http = new HttpPost(url);//Устанавливаем URL для POST-запроса.
            http.setEntity(new UrlEncodedFormEntity((data), HTTP.UTF_8));
            //http.setEntity(new UrlEncodedFormEntity(data));
            String response = httpclient.execute(http, new BasicResponseHandler());//Выполяем запрос
            Log.d("Debug",response);
            return response;//Возвращаем результат
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "200";
    }
    public static String auth(String user,String pass) {//Получаем логин и пароль
        List<NameValuePair> data = new ArrayList<NameValuePair>();//Создаём список данных
        data.add(new BasicNameValuePair("packet", "auth"));//Задаём заголовок пакета
        data.add(new BasicNameValuePair("login", user));// Записываем логин
        data.add(new BasicNameValuePair("pass", pass));// Записываем пароль
        String response = HttpR.send((ArrayList) data, API);
        if(!response.equals("200")){
            session = response;//Записываем сессию
            return "100";//Возвращаем код успешной операции
        }
        return "200";//Возвращаем код неудачной операции
    }
    public static String loadProlifeInfo() throws JSONException {
        List<NameValuePair> data = new ArrayList<NameValuePair>();//Создаём список данных
        data.add(new BasicNameValuePair("packet", "info"));//Задаём заголовок пакета
        data.add(new BasicNameValuePair("session",session));// Записываем сессию
        return HttpR.send((ArrayList) data, API);
    }
    public static String loadRepairs() throws JSONException {
        List<NameValuePair> data = new ArrayList<NameValuePair>();//Создаём список данных
        data.add(new BasicNameValuePair("packet", "repairs"));//Задаём заголовок пакета
        data.add(new BasicNameValuePair("session",session));// Записываем сессию
        return HttpR.send((ArrayList) data, API);
    }
    public static boolean checkauth() {
        List<NameValuePair> data = new ArrayList<NameValuePair>();
        data.add(new BasicNameValuePair("packet", "checkauth"));
        data.add(new BasicNameValuePair("session", session));
        String response = HttpR.send((ArrayList) data, API);
        if(response.equals("200"))
            return false;
        else
            return true;
    }
    public static boolean addinspect(String reason) throws UnsupportedEncodingException {
        List<NameValuePair> data = new ArrayList<NameValuePair>();
        data.add(new BasicNameValuePair("packet", "inspect"));
        data.add(new BasicNameValuePair("session", session));
//        data.add(new BasicNameValuePair("date",date));
        data.add(new BasicNameValuePair("reason",reason));
        String response = HttpR.send((ArrayList) data, API);
        if(response.equals("200"))
            return false;
        else
            return true;
    }

    public static String getrepair(String id) throws UnsupportedEncodingException {
        List<NameValuePair> data = new ArrayList<NameValuePair>();
        data.add(new BasicNameValuePair("packet", "getrepair"));
        data.add(new BasicNameValuePair("session", session));
        data.add(new BasicNameValuePair("id",id));
        String response = HttpR.send((ArrayList) data, API);
        return response;
    }

    public static String getNews() throws UnsupportedEncodingException {
        List<NameValuePair> data = new ArrayList<NameValuePair>();
        data.add(new BasicNameValuePair("packet", "news"));
        data.add(new BasicNameValuePair("session", session));
        String response = HttpR.send((ArrayList) data, API);
        return response;
    }
}



