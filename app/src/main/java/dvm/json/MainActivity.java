package dvm.json;

import android.app.Activity;
import android.os.StrictMode;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.widget.DrawerLayout;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

import static dvm.json.HttpR.*;


public class MainActivity extends ActionBarActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    public static final String FILENAME = "info";
    public static final String LOG_TAG = "Debug";
    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */

    private CharSequence mTitle;
    public View[] views = new View[3];
    public LinearLayout Buttons;
    public TextView result;
    public TextView result2;
    public EditText url;
    public EditText url2;
    public EditText url3;
    int wrapContent = LinearLayout.LayoutParams.WRAP_CONTENT;
    public List<NameValuePair> data = new ArrayList<NameValuePair>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
        views[0] = findViewById(R.id.tab1);
        views[1] = findViewById(R.id.tab2);
        views[2] = findViewById(R.id.tab3);
        Buttons = (LinearLayout) findViewById(R.id.buttons);
        result = (TextView) findViewById(R.id.result);
        result2 = (TextView) findViewById(R.id.result2);
        url = (EditText) findViewById(R.id.url);
        url2 = (EditText) findViewById(R.id.url2);
        url3 = (EditText) findViewById(R.id.url3);
        AnimationCare.selectTab(MainActivity.this,views[0],views);
        try {
            initList();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, PlaceholderFragment.newInstance(position + 1))
                .commit();
    }

    public void onSectionAttached(int number) {
        switch (number) {
            case 1:
                mTitle = getString(R.string.title_section1);
                AnimationCare.selectTab(MainActivity.this,views[0],views);
                break;
            case 2:
                mTitle = getString(R.string.title_section2);
                AnimationCare.selectTab(MainActivity.this,views[1],views);
                break;
            case 3:
                mTitle = getString(R.string.title_section3);
                AnimationCare.selectTab(MainActivity.this,views[2],views);
                break;
        }
    }
    public void clear(View v){
        writeFile("[]");
        Buttons.removeAllViews();
    }
    void writeFile(String text) {
        try {
            BufferedWriter bw = new BufferedWriter(new
                    OutputStreamWriter(openFileOutput (FILENAME,
                    MODE_PRIVATE)));
            bw.write(text);
            bw.close();
            Log.d(LOG_TAG, "Файл записан");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    String readFile() {
        String str="";

        try {
            BufferedReader br = new BufferedReader(new
                    InputStreamReader (openFileInput (FILENAME)));
            while ((str = br.readLine()) != null) {
                result2.setText(str);
                Log.d(LOG_TAG, str);
                return str;
            }
            Log.d(LOG_TAG, "Файл прочитан");

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "[]";
    }
    public void addNewButton(View v) throws JSONException {

        addButton(String.valueOf(url.getText()));
        addUrl(String.valueOf(url.getText()));

    }
    public void loadConfigFromURL(View v) throws JSONException {
        String list = HttpR.send((ArrayList) data,String.valueOf(url3.getText()));
        writeFile(list);
        reload(v);
    }
    public void reload(View v) throws JSONException {
        Buttons.removeAllViews();
        initList();
    }
    public void addButton(String url){
        Button btnNew = new Button(this);
        btnNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                open(v);
            }
        });
        btnNew.setText(url);
        LinearLayout.LayoutParams lParams = new LinearLayout.LayoutParams(
                wrapContent, wrapContent);
        lParams.setMargins(2,3,3,2);
        Buttons.addView(btnNew, lParams);
    }
    public void initList() throws JSONException {
        JSONArray jarr;
        String file=readFile();
        if(!file.equals("")){
            jarr = new JSONArray(file);
        }else{
            jarr = new JSONArray();
        }
        for (int i = 0; i < jarr.length();i++) {
            addButton(jarr.getString(i));
        }
    }
    public void addUrl(String url) throws JSONException {
        String file = readFile();
        JSONArray j;
        if(!file.equals("")){
            j = new JSONArray(file);
        }else{
            j = new JSONArray();
        }
        j.put(url);
        writeFile(j.toString());
        readFile();
    }
    public void add(View v) throws JSONException {
        addUrl(String.valueOf(url2.getText()));
        readFile();
    }
    public void open(View v){
        Button thisButton = (Button) v;
        List<NameValuePair> data = new ArrayList<NameValuePair>();//Создаём список данных
        String response;
        response = send((ArrayList) data, String.valueOf(thisButton.getText()));
        result.setText(Html.fromHtml(response));
        AnimationCare.selectTab(MainActivity.this,(View) views[1],views);
        readFile();
    }
    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            return rootView;
        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            ((MainActivity) activity).onSectionAttached(
                    getArguments().getInt(ARG_SECTION_NUMBER));
        }
    }

}
